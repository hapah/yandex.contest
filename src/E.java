import java.util.Scanner;

public class E {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s1 = scanner.nextLine();
        String s2 = scanner.nextLine();
        scanner.close();

        int len1 = s1.length();
        int len2 = s2.length();
        if (len1 != len2) {
            System.out.println(0);
            return;
        }
        byte[] alph1 = new byte[26];
        byte[] alph2 = new byte[26];

        for (int i = 0; i < len1; i++) {
            char c = s1.charAt(i);
            int ind = c - 97;
            alph1[ind] = (byte) (alph1[ind] + 1);
        }

        for (int i = 0; i < len1; i++) {
            char c = s2.charAt(i);
            int ind = c - 97;
            alph2[ind] = (byte) (alph2[ind] + 1);
        }

        for (int i = 0; i < 26; i++) {
            if (alph1[i] != alph2[i]) {
                System.out.println(0);
                return;
            }
        }

        System.out.println(1);
    }
}
