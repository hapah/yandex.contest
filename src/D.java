import java.util.Scanner;

public class D {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        scanner.close();
        recurse(n, 0, 0, "");
    }

    private static void recurse(int n, int counter_open, int counter_close, String ans) {
        if (counter_open + counter_close == 2 * n) {
            System.out.println(ans);
        } else {
            if (counter_open < n)
                recurse(n, counter_open + 1, counter_close, ans + "(");

            if (counter_open > counter_close)
                recurse(n, counter_open, counter_close + 1, ans + ")");
        }
    }
}
