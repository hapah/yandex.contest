import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Scanner;

public class F {
    public static void main(String[] args) throws FileNotFoundException {
        long beforeUsedMem = Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
        long beforeTime = System.currentTimeMillis();

        URL url = F.class.getResource("file");
        File file = new File(url.getPath());
        Scanner scanner = new Scanner(file);
        int k = Integer.parseInt(scanner.nextLine());
        StringBuilder arrays = new StringBuilder();
        for (int i = 0; i < k; i++) {
            arrays.append(scanner.nextLine()).append(" ");
        }
        int[] divs = new int[k];
        String[] array = arrays.toString().split(" ");
        int len = array.length;
        int divNum = 0;
        for (int i = 0; i < len - k; i++) {
            int min = 101;
            int l = 0;
            for (int j = 0; j < len; ) {
                int elem = Integer.parseInt(array[j]);
                if (elem != divs[l]) {
                    int cur = Integer.parseInt(array[j + 1 + divs[l]]);
                    if (cur < min) {
                        min = cur;
                        divNum = l;
                    }
                }
                j = elem + j + 1;
                l++;
            }
            divs[divNum] = divs[divNum] + 1;
            System.out.print(min + " ");
        }

        System.out.println();
        long afterTime = System.currentTimeMillis();
        long actualTime = afterTime - beforeTime;
        System.out.println("Time elapsed: " + actualTime);

        long afterUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
        long actualMemUsed=afterUsedMem-beforeUsedMem;
        System.out.println("Memory used: " + actualMemUsed);
    }
}
