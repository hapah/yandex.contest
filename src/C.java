import java.util.Scanner;

public class C {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String num;
        String lastnum = null;
        scanner.next();
        while (scanner.hasNext()) {
            num = scanner.next();
            if (!num.equals(lastnum)) {
                lastnum = num;
                System.out.println(lastnum);
            }
        }

        scanner.close();
    }
}