import java.io.*;

public class B {
    public static void main(String[] args) throws Exception {
        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));

        int result = 0;
        int len = 0;
        int n = Integer.parseInt(r.readLine());
        for (int i = 0; i < n; i++) {
            int num = Integer.parseInt(r.readLine());
            if (num == 1) {
                len++;
                if (len > result) {
                    result = len;
                }
            } else {
                len = 0;
            }
        }

        System.out.println(result);
    }
}